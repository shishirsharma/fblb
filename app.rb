require 'rubygems'

require 'bundler'
Bundler.require

require 'digest/sha1'
require 'date'

require 'dm-core'
require 'dm-timestamps'

require "will_paginate/data_mapper"

require_relative "app_helper"

DataMapper::Model.raise_on_save_failure = true
# DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite://#{Dir.pwd}/my.db")
DataMapper.setup(:default, ENV['DATABASE_URL'] || "postgres://vagrant:vagrant@localhost/vagrant")

# Auto load models
$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/models")
Dir.glob("#{File.dirname(__FILE__)}/models/*.rb") do |model|
  require File.basename(model, '.*')
end

DataMapper.finalize
DataMapper.auto_upgrade!

# To rename it into .html.erb
Tilt.register Tilt::ERBTemplate, 'html.erb'

# Filter
# before do
#   @user = {:login => "shishir"}
# end

# I exist!
get '/' do

  values = {}

  values[:defenders] = Player.all(:order => [:defender_points.desc],
                                  :limit => 10)

  values[:attackers] = Player.all(:order => [:attacker_points.desc],
                                  :limit => 10)

  values[:teams] = Team.all(:order => [:points.desc],
                            :limit => 10)

  erb :index, :locals => values
end

get '/match/new' do
  erb :new_match
end

post '/match/new' do

puts params.inspect
  if params[:scoreBlue].to_i >  params[:scoreRed].to_i
    score_blue = 1
    score_red = -1
  else
    score_blue = -1
    score_red = 1
  end

  blue_at = Player.first_or_create( :name => params[:blue_attacker] )
  blue_at.attacker_points += score_blue

  blue_at.save

  blue_df = Player.first_or_create( :name => params[:blue_defender] )
  blue_df.defender_points += score_blue
  blue_df.save

  team_blue = Team.first_or_create({ :attacker_id => blue_at.id, :defender_id => blue_df.id })
  team_blue.points += score_blue*2
  team_blue.save

  red_at = Player.first_or_create( :name => params[:red_attacker] )
  red_at.attacker_points += score_red
  red_at.save

  red_df = Player.first_or_create( :name => params[:red_defender] )
  red_df.defender_points += score_red
  red_df.save

  team_red = Team.first_or_create({ :attacker_id => red_at.id, :defender_id => red_df.id })
  team_red.points += score_red*2
  team_red.save

  if score_blue > 0
    winner = team_blue
  else
    winner = team_red
  end

  match = Match.new( :red => team_red,
                     :red_score => params[:scoreRed].to_i,

                     :blue => team_blue,
                     :blue_score => params[:scoreBlue].to_i,

                     :start_time => Time.now,
                     :duration =>  10*60,

                     :winner => winner,
                     :duration => '10' )
  match.save

  redirect '/'
end


delete '/player/:id' do |id|
  Player.get(params[:id]).destroy
end

get '/teams' do
  values = {}
  values[:teams] = Team.paginate(:order => [:created_at.asc],
                                    :page => params[:page],
                                    :per_page => 10)
  erb :teams, :locals => values
end

get '/team/:id' do
  result = Team.get(params[:id]) if params[:id].nil?
  result.to_json
end

delete '/team/:id' do
  Team.get(params[:id]).destroy
end


get '/matches' do
  values = {}
  values[:matches] = Match.paginate(:order => [:created_at.desc],
                                    :page => params[:page],
                                    :per_page => 10)
  erb :matches, :locals => values
end

get '/players' do
  values = {}
  values[:players] = Player.paginate(:order => [:created_at.desc],
                                    :page => params[:page],
                                    :per_page => 10)
  erb :players, :locals => values
end


get '/matches/:id' do
  values = {}
  values[:matches] = Match.all(:red_id => params[:id]) + Match.all(:blue_id => params[:id])
  
  erb :matches, :locals => values
end

