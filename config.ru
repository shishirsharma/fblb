require 'rubygems'
require 'bundler'
require './app'

Bundler.require(:default, ENV['RACK_ENV'].to_sym)

enable :logging
disable :protection

run Sinatra::Application
