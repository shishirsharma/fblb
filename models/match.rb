
class Match
  include DataMapper::Resource

  property :id, Serial

  belongs_to :red, :model => 'Team'
  belongs_to :blue, :model => 'Team'

  property :red_score, Integer, :required => true
  property :blue_score, Integer, :required => true

  property :start_time, Time
  property :duration, Integer

  belongs_to :winner, :model => 'Team'

  property :created_at, DateTime, :index => true
  property :updated_at, DateTime, :index => true

end
