
class Team
  include DataMapper::Resource

  property :id, Serial

  belongs_to :attacker, :model => 'Player'
  belongs_to :defender, :model => 'Player'

  property :points, Integer, :default => 50
  property :name, String

  property :created_at, DateTime, :index => true
  property :updated_at, DateTime, :index => true

  has n, :match, :child_key => [:red_id, :blue_id, :winner_id]

end
